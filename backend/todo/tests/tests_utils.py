import pytest
from todo.utils import add


def tests_add():
    actual = add(1, 2)
    assert actual == 3
